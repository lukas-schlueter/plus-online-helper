pub struct Browser {
    pub client: user_agent::ReqwestSession,
}

impl Default for Browser {
    fn default() -> Self {
        use reqwest::RedirectPolicy;
        Self {
            client: user_agent::ReqwestSession::new(
                reqwest::Client::builder()
                    .redirect(RedirectPolicy::none())
                    .build()
                    .unwrap(),
            ),
        }
    }
}

impl Browser {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn follow_redirects(
        &mut self,
        url: &str,
    ) -> Result<reqwest::Response, user_agent::ReqwestSessionError> {
        let mut response = self.client.get(url);
        loop {
            if response.is_err() {
                return response;
            }

            let resp = response.unwrap();

            let location: Option<&reqwest::header::HeaderValue> =
                resp.headers().get(reqwest::header::LOCATION);
            if location.is_none() {
                return Ok(resp);
            }

            let target = location.unwrap();
            let url = resp.url().join(target.to_str().unwrap()).unwrap();
            response = self.client.get(url);
        }
    }
}
