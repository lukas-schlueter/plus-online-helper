use console::Emoji;
use dialoguer::{theme::ColorfulTheme, Input, PasswordInput};
use quicli::prelude::*;
use scraper::{Html, Selector};
use structopt::StructOpt;

use super::PlusOnline;

#[derive(StructOpt, Debug)]
pub struct Config {
    #[structopt()]
    username: Option<String>,
    #[structopt()]
    password: Option<String>,
    #[structopt(flatten)]
    pub verbosity: Verbosity,
}

pub struct Cli {
    config: Config,
    client: PlusOnline,
}

static KEY: Emoji = Emoji("\u{1f511} ", "");

impl Cli {
    pub fn new(config: Config) -> Self {
        Self {
            config,
            client: PlusOnline::default(),
        }
    }

    pub fn login(&mut self) -> Result<bool, Error> {
        let username = self.get_username().unwrap();
        let password = self.get_password().unwrap();

        println!("{}Logging you in...", KEY);

        let loggedin = match self.client.login(&username, &password) {
            Ok(success) => success,
            Err(e) => {
                error!("Failed to login. Error: {}", e);
                return Err(e)?;
            }
        };

        if loggedin {
            self.config.username = Some(username);
            self.config.password = Some(password);
        }

        Ok(loggedin)
    }

    pub fn fees(&mut self) -> Result<(), Error> {
        let mut response = self.client.tuition_fees()?;
        let document = Html::parse_document(response.text()?.as_str());
        debug!("{:#?}", document.errors);

        let open = document
            .select(&Selector::parse(".z0 > td:nth-child(3) > span").unwrap())
            .next()
            .unwrap()
            .inner_html();
        let total = document
            .select(&Selector::parse(".z0 > td:nth-child(2) > span").unwrap())
            .next()
            .unwrap()
            .inner_html();
        println!("Tuition Fees");
        println!("------------");
        println!("Open: {}/{} \u{20ac}", open, total);
        Ok(())
    }

    fn get_username(&self) -> Result<String, std::io::Error> {
        if let Some(username) = &self.config.username {
            Ok(username.to_string())
        } else {
            Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Username")
                .interact()
        }
    }

    fn get_password(&self) -> Result<String, std::io::Error> {
        if let Some(password) = &self.config.password {
            Ok(password.to_string())
        } else {
            PasswordInput::with_theme(&ColorfulTheme::default())
                .with_prompt("Password (hidden)")
                .interact()
        }
    }
}
