#[macro_use]
extern crate failure;

use std::collections::HashMap;

use quicli::prelude::log::*;
use reqwest::Response;

use browser::Browser;

mod browser;
pub mod cli;

pub struct PlusOnline {
    browser: Browser,
    loggedin: bool,
}

impl Default for PlusOnline {
    fn default() -> Self {
        Self {
            browser: Browser::new(),
            loggedin: false,
        }
    }
}

#[derive(Debug, Fail)]
enum PlusOnlineError {
    #[fail(display = "missing http header: {}", name)]
    MissingHeader { name: String },
}

impl PlusOnline {
    pub fn tuition_fees(&mut self) -> Result<Response, failure::Error> {
        let response = self
            .browser
            .client
            .get("https://online.uni-salzburg.at/plus_online/wbstudienbeitragstatus.show")?;
        Ok(response)
    }

    pub fn login(&mut self, username: &str, password: &str) -> Result<bool, failure::Error> {
        let mut data = HashMap::new();
        data.insert("ctxid", "");
        data.insert("curl", "");
        data.insert("cinframe", "");
        data.insert("cp1", username);
        data.insert("cp2", password);
        data.insert("cUserGroup", "");

        let mut response = self
            .browser
            .follow_redirects("https://online.uni-salzburg.at/plus_online/webnav.ini")?;
        debug!("{:#?}", response);

        response = self.browser.follow_redirects(
            "https://online.uni-salzburg.at/plus_online/anmeldung.durchfuehren",
        )?;
        debug!("{:#?}", response);

        response = self.browser.client.post_with(
            "https://online.uni-salzburg.at/plus_online/wbAnmeldung.durchfuehren",
            |builder| builder.form(&data),
        )?;
        debug!("{:#?}", response);

        let location = if let Some(location) = response.headers().get("location") {
            response.url().join(location.to_str().unwrap()).unwrap()
        } else {
            return Err(PlusOnlineError::MissingHeader {
                name: String::from("Location"),
            })?;
        };

        self.loggedin = location.as_str().contains("loginsuccess");
        response = self.browser.follow_redirects(location.as_str())?;
        debug!("{:#?}", response);

        Ok(self.loggedin)
    }
}
