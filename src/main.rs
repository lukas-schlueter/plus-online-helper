extern crate dialoguer;
#[macro_use]
extern crate human_panic;

use console::{style, Term};
use dialoguer::{theme::ColorfulTheme, Select};
use quicli::prelude::*;
use structopt::StructOpt;

use plus_online_helper::cli::{Cli, Config};

fn main() -> CliResult {
    if !cfg!(debug_assertions) {
        setup_panic!();
    }
    let args = Config::from_args();
    args.verbosity.setup_env_logger(env!("CARGO_PKG_NAME"))?;
    debug!("{:#?}", args);

    println!("===================================");
    println!("= Welcome to the PlusOnline tool! =");
    println!("===================================");

    let cli = &mut Cli::new(args);

    while !cli.login()? {
        println!("Invalid login data!");
    }

    println!("{}", style("Login successful!").green());

    dont_disappear::any_key_to_continue::default();
    menu(cli);
    dont_disappear::any_key_to_continue::default();
    Ok(())
}

fn menu(cli: &mut Cli) {
    let term = Term::stdout();
    let menu_items = vec![("Tuition Fees", |cli: &mut Cli| cli.fees())];
    let mut keys: Vec<&str> = menu_items.iter().map(|item| item.0).collect();
    keys.insert(keys.len(), "Exit");

    loop {
        term.clear_screen().unwrap();

        let selection = Select::with_theme(&ColorfulTheme::default())
            .with_prompt("What do you want to do?")
            .items(&keys)
            .default(0)
            .interact()
            .unwrap();

        if selection == keys.len() - 1 {
            break;
        }

        menu_items[selection].1(cli).unwrap();
        dont_disappear::any_key_to_continue::custom_msg("Press any key to get back to the menu")
    }
}
